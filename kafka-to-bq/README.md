## DirectRunner

### Kafka to Bigquery
```bash
mvn compile exec:java -Dexec.mainClass=com.aldouse.KafkaToBQ "-Dexec.args=\
     --runner=DirectRunner \
     --kafkaBrokers=broker1:9092,broker2:9092,broker3:9092  \
     --kafkaTopics=topic1 \
     --kafkaConsumerConfigGroupID=detest0 \
     --kafkaConsumerConfigEnableAutoCommitConfig=true \
     --kafkaConsumerConfigAutoCommitIntervalMs=5 \
     --kafkaConsumerConfigAutoOffsetReset=earliest \
     --defaultGcsBucket=dev \
     --bqProjectID=dev \
     --bqDatasetID=datalake \
     --project=dev" 
```

### Kafka to GCS
```bash
mvn compile exec:java -Dexec.mainClass=com.aldouse.KafkaToGCS "-Dexec.args=\
     --runner=DirectRunner \
     --kafkaBrokers=broker1:9092,broker2:9092,broker3:9092  \
     --kafkaTopics=topic1 \
     --kafkaConsumerConfigGroupID=detest1 \
     --kafkaConsumerConfigEnableAutoCommitConfig=true \
     --kafkaConsumerConfigAutoCommitIntervalMs=100 \
     --kafkaConsumerConfigAutoOffsetReset=earliest \
     --defaultGcsBucket=dev \
     --project=dev" 
```


package com.aldouse.constant;

public class Constant {

    public static final String GCS_OUTPUT = "gcs";
    public static final String BIGQUERY_OUTPUT = "bigquery";

    public static final String WORKSPACE = "beam";

    public static final String REPEATED = "REPEATED";
    public static final String NULLABLE = "NULLABLE";
}

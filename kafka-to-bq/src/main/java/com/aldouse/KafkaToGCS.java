package com.aldouse;

import com.aldouse.coder.TMRecordCoder;
import com.aldouse.constant.Constant;
import com.aldouse.io.TMRecordSink;
import com.aldouse.options.CustomOptions;
import com.aldouse.tm.TMRecord;
import com.aldouse.transformation.ConvertKafkaRecordToTMRecord;
import com.aldouse.util.GCSFileNaming;
import com.google.common.collect.ImmutableMap;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.CoderRegistry;
import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.io.kafka.KafkaIO;
import org.apache.beam.sdk.io.kafka.KafkaRecord;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.transforms.windowing.FixedWindows;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionTuple;
import org.apache.beam.sdk.values.TupleTag;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.joda.time.Duration;

import java.util.Arrays;

public class KafkaToGCS {

    final static TupleTag<TMRecord> validRecords = new TupleTag<TMRecord>() {
    };
    final static TupleTag<TMRecord> invalidRecords = new TupleTag<TMRecord>() {
    };

    final static String outputTarget = Constant.GCS_OUTPUT;


    public static void main(String[] args) {

        CustomOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(CustomOptions.class);

        Pipeline p = Pipeline.create(options);
        CoderRegistry cr = p.getCoderRegistry();
        cr.registerCoderForClass(TMRecord.class, new TMRecordCoder());

        PCollection<KafkaRecord<String, String>> kafkaRecords = p.apply("ConsumeTopics", KafkaIO.<String, String>read()
                .withBootstrapServers(options.getKafkaBrokers())
                .withTopics(Arrays.asList(options.getKafkaTopics().split(",")))
                .withReadCommitted()
                .withConsumerConfigUpdates(new ImmutableMap.Builder<String, Object>()
                        .put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, options.getKafkaConsumerConfigEnableAutoCommitConfig())
                        .put(ConsumerConfig.GROUP_ID_CONFIG, options.getKafkaConsumerConfigGroupID())
                        .put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, options.getKafkaConsumerConfigAutoCommitIntervalMs())
                        .put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, options.getKafkaConsumerConfigAutoOffsetReset()).build())
                .withKeyDeserializer(StringDeserializer.class)
                .withValueDeserializer(StringDeserializer.class)
        );

        String gcsWorkspace = Constant.WORKSPACE;
        String gcsPrefix = String.format("gs://%s/%s", options.getDefaultGcsBucket(), gcsWorkspace);
        String gcsValidTempLocation = String.format("%s/gcs_temp/valid/", gcsPrefix);
        String gcsInvalidTempLocation = String.format("%s/gcs_temp/invalid/", gcsPrefix);
        String gcsDatalakeLocation = String.format("%s/%s/", gcsPrefix, options.getGcsDatalakeLocation());
        String gcsInvalidLocation = String.format("%s/%s/", gcsPrefix, options.getGcsInvalidLocation());
        String topicBQSchemasLocation = String.format("%s/%s", gcsPrefix, options.getTopicBQSchemasLocation());


        PCollectionTuple tmRecords = kafkaRecords.apply("ConvertKafkaRecordToTMRecord",
                ConvertKafkaRecordToTMRecord.create()
                        .setRedisHost(options.getRedisHost())
                        .setValidTag(validRecords)
                        .setInvalidTag(invalidRecords)
                        .setSchemaDir(topicBQSchemasLocation)
//                        .setOutputTarget(outputTarget)
                        .build()
        );

        tmRecords.get(validRecords)
                .apply(Window.into(
                        FixedWindows.of(Duration.standardSeconds(10))))
                .apply("WriteToGCS", FileIO.<String, TMRecord>writeDynamic()
                        .by((SerializableFunction<TMRecord, String>) TMRecord::getPartitionByDateHour)
                        .via(new TMRecordSink())
                        .withDestinationCoder(StringUtf8Coder.of())
                        .withNumShards(4)
                        .withTempDirectory(gcsValidTempLocation)
                        .withNaming(datePartition ->
                                GCSFileNaming.getNaming(gcsDatalakeLocation + datePartition, "json")));

        tmRecords.get(invalidRecords)
                .apply(Window.into(
                        FixedWindows.of(Duration.standardSeconds(10))))
                .apply("WriteInvalidToGCS", FileIO.<String, TMRecord>writeDynamic()
                        .by((SerializableFunction<TMRecord, String>) TMRecord::getPartitionByDateHour)
                        .via(new TMRecordSink())
                        .withDestinationCoder(StringUtf8Coder.of())
                        .withNumShards(4)
                        .withTempDirectory(gcsInvalidTempLocation)
                        .withNaming(datePartition ->
                                GCSFileNaming.getNaming(gcsInvalidLocation + datePartition, "json")));

        p.run().waitUntilFinish();


    }
}

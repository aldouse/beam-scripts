package com.aldouse;

import com.aldouse.coder.BigqueryInsertErrorWrapperCoder;
import com.aldouse.coder.TMRecordCoder;
import com.aldouse.constant.Constant;
import com.aldouse.io.BQESink;
import com.aldouse.io.DynamicBigqueryTMDestination;
import com.aldouse.io.TMRecordSink;
import com.aldouse.options.CustomOptions;
import com.aldouse.tm.TMRecord;
import com.aldouse.transformation.ConvertKafkaRecordToTMRecord;
import com.aldouse.transformation.TMRecordToTableRowFormatter;
import com.aldouse.transformation.WrapBigqueryInsertError;
import com.aldouse.util.GCSFileNaming;
import com.aldouse.wrapper.BigQueryInsertErrorWrapper;
import com.google.common.collect.ImmutableMap;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.CoderRegistry;
import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryInsertError;
import org.apache.beam.sdk.io.gcp.bigquery.InsertRetryPolicy;
import org.apache.beam.sdk.io.kafka.KafkaIO;
import org.apache.beam.sdk.io.kafka.KafkaRecord;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.transforms.windowing.FixedWindows;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionTuple;
import org.apache.beam.sdk.values.TupleTag;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.joda.time.Duration;

import java.util.Arrays;

public class KafkaToBQ {

    final static TupleTag<TMRecord> validRecords = new TupleTag<>() {
    };
    final static TupleTag<TMRecord> invalidRecords = new TupleTag<>() {
    };

    final static String outputTarget = Constant.BIGQUERY_OUTPUT;


    public static void main(String[] args) {

        CustomOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(CustomOptions.class);

        Pipeline p = Pipeline.create(options);
        CoderRegistry cr = p.getCoderRegistry();
        cr.registerCoderForClass(TMRecord.class, new TMRecordCoder());
        cr.registerCoderForClass(BigQueryInsertErrorWrapper.class, new BigqueryInsertErrorWrapperCoder());

        PCollection<KafkaRecord<String, String>> kafkaRecords = p.apply("ConsumeTopics", KafkaIO.<String, String>read()
                .withBootstrapServers(options.getKafkaBrokers())
                .withTopics(Arrays.asList(options.getKafkaTopics().split(",")))
                .withReadCommitted()
                .withConsumerConfigUpdates(new ImmutableMap.Builder<String, Object>()
                        .put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, options.getKafkaConsumerConfigEnableAutoCommitConfig())
                        .put(ConsumerConfig.GROUP_ID_CONFIG, options.getKafkaConsumerConfigGroupID())
                        .put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, options.getKafkaConsumerConfigAutoCommitIntervalMs())
                        .put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, options.getKafkaConsumerConfigAutoOffsetReset()).build())
                .withKeyDeserializer(StringDeserializer.class)
                .withValueDeserializer(StringDeserializer.class)
        );

        String gcsWorkspace = Constant.WORKSPACE;
        String gcsPrefix = String.format("gs://%s/%s", options.getDefaultGcsBucket(), gcsWorkspace);
        String bigqueryGcsTempLocation = String.format("%s/bq_temp/", gcsPrefix);
        String gcsInvalidTempLocation = String.format("%s/gcs_temp/invalid/", gcsPrefix);
        String gcsInvalidLocation = String.format("%s/%s/", gcsPrefix, options.getGcsInvalidLocation());
        String gcsBQFailedLocation = String.format("%s/%s/", gcsPrefix, options.getGcsBQFailedLocation());
        String topicBQSchemasLocation = String.format("%s/%s", gcsPrefix, options.getTopicBQSchemasLocation());


        PCollectionTuple tmRecords = kafkaRecords.apply("ConvertKafkaRecordToTMRecord",
                ConvertKafkaRecordToTMRecord.create()
                        .setRedisHost(options.getRedisHost())
                        .setValidTag(validRecords)
                        .setInvalidTag(invalidRecords)
                        .setSchemaDir(topicBQSchemasLocation)
                        .build()
        );

        PCollection<BigQueryInsertError> failedBQInserts = tmRecords.get(validRecords).apply("writeToBigQuery", BigQueryIO.<TMRecord>write().
                        to(new DynamicBigqueryTMDestination(
                                options.getBqProjectID(),
                                options.getBqDatasetID(),
                                options.getBqPartitionType(),
                                options.getBqPartitionField()))
                        .withFormatFunction(TMRecordToTableRowFormatter.create()
                                .setPartitionField(options.getBqPartitionField()))
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
                        .withMethod(BigQueryIO.Write.Method.STREAMING_INSERTS)
//                        .withMethod(BigQueryIO.Write.Method.FILE_LOADS)
//                        .withNumStorageWriteApiStreams(10) // TODO: figure out the number for this
//                        .withTriggeringFrequency(Duration.standardSeconds(options.getBqLoadJobIntervalSeconds()))
                        .withCustomGcsTempLocation(ValueProvider.StaticValueProvider.of(bigqueryGcsTempLocation))
                        .withFailedInsertRetryPolicy(InsertRetryPolicy.retryTransientErrors())
                        .withExtendedErrorInfo())
                .getFailedInsertsWithErr();

        failedBQInserts
                .apply("WrapBQInsertError", WrapBigqueryInsertError.create().build())
                .apply(Window.into(FixedWindows.of(Duration.standardSeconds(10))))
                .apply("writeFailedToGCS", FileIO.<String, BigQueryInsertErrorWrapper>writeDynamic()
                        .by((SerializableFunction<BigQueryInsertErrorWrapper, String>) BigQueryInsertErrorWrapper::getPartitionByDateHour)
                        .via(new BQESink())
                        .withDestinationCoder(StringUtf8Coder.of())
                        .withNumShards(4)
                        .withTempDirectory(gcsInvalidTempLocation)
                        .withNaming(datePartition ->
                                GCSFileNaming.getNaming(gcsBQFailedLocation + datePartition, "json"))
                );


        tmRecords.get(invalidRecords)
                .apply(Window.into(FixedWindows.of(Duration.standardSeconds(10))))
                .apply("WriteInvalidToGCS", FileIO.<String, TMRecord>writeDynamic()
                        .by((SerializableFunction<TMRecord, String>) TMRecord::getPartitionByDateHour)
                        .via(new TMRecordSink())
                        .withDestinationCoder(StringUtf8Coder.of())
                        .withNumShards(4)
                        .withTempDirectory(gcsInvalidTempLocation)
                        .withNaming(datePartition ->
                                GCSFileNaming.getNaming(gcsInvalidLocation + datePartition, "json")));

        p.run().waitUntilFinish();


    }
}

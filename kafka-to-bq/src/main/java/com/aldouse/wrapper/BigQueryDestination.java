package com.aldouse.wrapper;

import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.coders.DefaultCoder;

import java.util.Objects;

/**
 * Wrapper class for destination when saving to BigQuery
 */
@DefaultCoder(AvroCoder.class)
public class BigQueryDestination {

    private String schema; //  schema in string format
    private String key;

    public BigQueryDestination() {
    }

    public BigQueryDestination(String schema, String key) {
        this.schema = schema;
        this.key = key;
    }

    public String getSchema() {
        return schema;
    }

    public BigQueryDestination setSchema(String schema) {
        this.schema = schema;
        return this;
    }

    public String getKey() {
        return key;
    }

    public BigQueryDestination setKey(String key) {
        this.key = key;
        return this;
    }

    @Override
    public String toString() {
        return "BigQueryDestination{" +
                "schema='" + schema + '\'' +
                ", key='" + key + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BigQueryDestination that = (BigQueryDestination) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
package com.aldouse.wrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


public class BigQueryInsertErrorWrapper  {

    static Logger LOG = LoggerFactory.getLogger(BigQueryInsertErrorWrapper.class);
    public static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public String getDatasetID() {
        return DatasetID;
    }

    public String getTableID() {
        return TableID;
    }

    private  String DatasetID;
    private  String TableID;
    private  String ErrorMsg;
    private String RowJson;


    public BigQueryInsertErrorWrapper(String datasetID, String tableID, String errorMsg, String rowJson) {
        this.DatasetID = datasetID;
        this.TableID = tableID;
        this.ErrorMsg = errorMsg;
        this.RowJson = rowJson;
    }


    public String getPartitionByDateHour() {
        LocalDateTime dt = Instant.now().atZone(ZoneId.of("UTC")).toLocalDateTime();

        return String.format("%s.%s/dt=%s/hour=%s",
                this.DatasetID,
                this.TableID,
                dt.format(dateFormat),
                dt.getHour()
        );
    }

    public String getErrorMsg() {
        return ErrorMsg;
    }

    public String getRowJson() {
        return RowJson;
    }
}

package com.aldouse.coder;

import com.aldouse.tm.TMRecord;
import org.apache.beam.sdk.coders.CustomCoder;
import org.apache.beam.sdk.coders.NullableCoder;
import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.coders.VarLongCoder;

import java.io.*;

public class TMRecordCoder extends CustomCoder<TMRecord> {
    private static final NullableCoder<String> STRING_CODER = NullableCoder.of(StringUtf8Coder.of());
    private static final NullableCoder<Long> LONG_CODER = NullableCoder.of(VarLongCoder.of());


    public TMRecordCoder() {
    }


    @Override
    public void encode(TMRecord value, OutputStream outStream) throws IOException {
        assert value != null;

        STRING_CODER.encode(value.getTopic(), outStream);
        STRING_CODER.encode(value.getKey(), outStream);
        STRING_CODER.encode(value.getValue(), outStream);
        STRING_CODER.encode(value.getSchema(), outStream);
        LONG_CODER.encode(value.getKafkaTimestamp(), outStream);
        LONG_CODER.encode(value.getProcessedTimestamp(), outStream);
    }


    @Override
    public TMRecord decode(InputStream inStream) throws IOException {

        String topic = STRING_CODER.decode(inStream);
        String key = STRING_CODER.decode(inStream);
        String value = STRING_CODER.decode(inStream);
        String schema = STRING_CODER.decode(inStream);
        Long kafkaTimestamp = LONG_CODER.decode(inStream);
        Long processedTimestamp = LONG_CODER.decode(inStream);

        return new TMRecord(topic, key, value, kafkaTimestamp)
                .setSchema(schema)
                .setProcessedTimestamp(processedTimestamp);
    }


}

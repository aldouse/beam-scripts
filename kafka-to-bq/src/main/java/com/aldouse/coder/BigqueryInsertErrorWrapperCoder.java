package com.aldouse.coder;

import com.aldouse.tm.TMRecord;
import com.aldouse.wrapper.BigQueryInsertErrorWrapper;
import org.apache.beam.sdk.coders.CustomCoder;
import org.apache.beam.sdk.coders.NullableCoder;
import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.coders.VarLongCoder;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BigqueryInsertErrorWrapperCoder extends CustomCoder<BigQueryInsertErrorWrapper> {
    private static final NullableCoder<String> STRING_CODER = NullableCoder.of(StringUtf8Coder.of());


    public BigqueryInsertErrorWrapperCoder() {
    }


    @Override
    public void encode(BigQueryInsertErrorWrapper value, OutputStream outStream) throws IOException {
        assert value != null;

        STRING_CODER.encode(value.getDatasetID(), outStream);
        STRING_CODER.encode(value.getTableID(), outStream);
        STRING_CODER.encode(value.getErrorMsg(), outStream);
        STRING_CODER.encode(value.getRowJson(), outStream);

    }


    @Override
    public BigQueryInsertErrorWrapper decode(InputStream inStream) throws IOException {

        String datasetID = STRING_CODER.decode(inStream);
        String tableID = STRING_CODER.decode(inStream);
        String errorMsg = STRING_CODER.decode(inStream);
        String rowJson = STRING_CODER.decode(inStream);

        return new BigQueryInsertErrorWrapper(datasetID, tableID, errorMsg, rowJson);
    }

}

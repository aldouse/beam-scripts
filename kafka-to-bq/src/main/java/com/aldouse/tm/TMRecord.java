package com.aldouse.tm;

import com.aldouse.util.GCSFileNaming;
import org.apache.beam.sdk.io.kafka.KafkaRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class TMRecord {
    static Logger LOG = LoggerFactory.getLogger(TMRecord.class);
    public static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private String topic;
    private Long kafkaTimestamp;
    private Long processedTimestamp;
    private String key;
    private String value;
    private String schema;


    public TMRecord(String topic, String key, String value, long kafkaTimestamp) {
        this.topic = topic;
        this.key = key;
        this.value = value;
        this.kafkaTimestamp = kafkaTimestamp;
    }

    public String getPartitionByDateHour() {
        LocalDateTime dt = Instant.ofEpochMilli(this.processedTimestamp).atZone(ZoneId.of("UTC")).toLocalDateTime();

        String dtHour = String.format("%s/dt=%s/hour=%s", this.topic, dt.format(dateFormat), dt.getHour());
        return dtHour;
    }


    public String getTopic() {
        return topic;
    }

    public TMRecord setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public long getKafkaTimestamp() {
        return kafkaTimestamp;
    }

    public TMRecord setKafkaTimestamp(long kafkaTimestamp) {
        this.kafkaTimestamp = kafkaTimestamp;
        return this;
    }

    public long getProcessedTimestamp() {
        return processedTimestamp;
    }

    public TMRecord setProcessedTimestamp(long processedTimestamp) {
        this.processedTimestamp = processedTimestamp;
        return this;
    }

    public String getKey() {
        return key;
    }

    public TMRecord setKey(String key) {
        this.key = key;
        return this;
    }

    public String getValue() {
        return value;
    }

    public TMRecord setValue(String value) {
        this.value = value;
        return this;
    }

    public String getSchema() {
        return schema;
    }

    public TMRecord setSchema(String schema) {
        this.schema = schema;
        return this;
    }

}

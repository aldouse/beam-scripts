package com.aldouse.transformation;

import com.aldouse.wrapper.BigQueryInsertErrorWrapper;
import com.google.api.services.bigquery.model.TableReference;
import com.google.gson.Gson;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryInsertError;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WrapBigqueryInsertError extends DoFn<BigQueryInsertError, BigQueryInsertErrorWrapper> {

    static Logger LOG = LoggerFactory.getLogger(WrapBigqueryInsertError.class);

    public WrapBigqueryInsertError() {
    }

    public static WrapBigqueryInsertError create() {
        return new WrapBigqueryInsertError();
    }

    public ParDo.SingleOutput<BigQueryInsertError, BigQueryInsertErrorWrapper> build() {
        return ParDo.of(this);
    }

    @ProcessElement
    public void processElement(ProcessContext ctx) {
        BigQueryInsertError bqe = ctx.element();
        TableReference tb = bqe.getTable();
        LOG.error(String.format("BigqueryFailedInsert: %s.%s Error: %s ",
                tb.getDatasetId(),
                tb.getTableId(),
                bqe.getError())
        );

        Gson gson = new Gson();

        ctx.output(new BigQueryInsertErrorWrapper(
                bqe.getTable().getDatasetId(),
                bqe.getTable().getTableId(),
                bqe.getError().toString(),
                gson.toJson(bqe.getRow())
        ));

    }
}

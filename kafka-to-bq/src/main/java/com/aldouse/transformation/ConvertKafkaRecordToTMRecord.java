package com.aldouse.transformation;

import com.aldouse.constant.Constant;
import com.aldouse.tm.TMRecord;
import com.aldouse.util.LogUtils;
import com.aldouse.util.RedisUtils;
import com.aldouse.util.SchemaUtils;
import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.gson.*;
import org.apache.beam.sdk.io.kafka.KafkaRecord;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.TupleTag;
import org.apache.beam.sdk.values.TupleTagList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.*;

public class ConvertKafkaRecordToTMRecord extends DoFn<KafkaRecord<String, String>, TMRecord> {
    static Logger LOG = LoggerFactory.getLogger(ConvertKafkaRecordToTMRecord.class);
    private RedisUtils redis;
    private final String className = "ConvertKafkaRecordToTMRecord";
    final long localCacheTtlMillis = 15 * 60 * 1000;
    private TupleTag<TMRecord> validTag;
    private TupleTag<TMRecord> invalidTag;
    private String redisHost;
    private String schemaDir;


    public ConvertKafkaRecordToTMRecord() {
    }

    public String getSchemaDir() {
        return schemaDir;
    }

    public String getRedisHost() {
        return redisHost;
    }

    public ConvertKafkaRecordToTMRecord setSchemaDir(String schemaDir) {
        this.schemaDir = schemaDir;
        return this;
    }

    public ConvertKafkaRecordToTMRecord setRedisHost(String redisHost) {
        this.redisHost = redisHost;
        return this;
    }

    public ConvertKafkaRecordToTMRecord setValidTag(TupleTag<TMRecord> validTag) {
        this.validTag = validTag;
        return this;
    }

    public ConvertKafkaRecordToTMRecord setInvalidTag(TupleTag<TMRecord> invalidTag) {
        this.invalidTag = invalidTag;
        return this;
    }


    public static ConvertKafkaRecordToTMRecord create() {
        return new ConvertKafkaRecordToTMRecord();
    }

    public ParDo.MultiOutput<KafkaRecord<String, String>, TMRecord> build() {
        return ParDo.of(this).withOutputTags(this.validTag,
                TupleTagList.of(this.invalidTag));
    }

    @Setup
    public void setup() {
        RedisUtils redis = new RedisUtils(this.redisHost);
        if (redis.pool == null) {
            redis.setLocalCache(className, localCacheTtlMillis);
            redis.initRedis();
        }
        this.redis = redis;
    }

    @Teardown
    public void teardown() {
        this.redis.pool.close();
    }


    private List<TableFieldSchema> fixTFSTypeErasure(List<TableFieldSchema> fields) {
        Gson gson = new Gson();
        return Arrays.asList(gson.fromJson(gson.toJson(fields), TableFieldSchema[].class));
    }

    private void findMapOfStringField(List<List<String>> mapOfStringFields, List<String> prevField, List<TableFieldSchema> fields) {

        for (TableFieldSchema field : fields) {
            String currentFieldName;
            String currentFieldMode;
            if (!Objects.equals(field.getType(), "RECORD")) {
                continue;
            }

            List<TableFieldSchema> innerFields = fixTFSTypeErasure(field.getFields());

            if (prevField.isEmpty()) {
                currentFieldName = field.getName();
                currentFieldMode = field.getMode();
            } else {
                currentFieldName = prevField.get(0) + "." + field.getName();
                currentFieldMode = prevField.get(1) + "." + field.getMode();
            }
            List<String> currentField = Arrays.asList(currentFieldName, currentFieldMode);

            if (field.getFields().size() == 2) {

                List<String> validFieldNames = new ArrayList<>(List.of("key", "value"));

                for (TableFieldSchema currentCheckedField : innerFields) {
                    boolean success = validFieldNames.remove(currentCheckedField.getName());
                    if (!success) {
                        break;
                    }
                }

                if (validFieldNames.isEmpty()) {
                    mapOfStringFields.add(currentField);
                }
            }

            findMapOfStringField(mapOfStringFields, currentField, innerFields);
        }
    }


    private JsonArray updateMapOfStringObj(JsonObject value) {
        List<String> keys = new ArrayList<>(value.keySet());
        JsonArray newValue = new JsonArray();
        for (String key : keys) {
            JsonObject kv = new JsonObject();
            kv.addProperty("key", key);
            kv.addProperty("value", value.get(key).getAsString());
            newValue.add(kv);
        }
        return newValue;
    }

    // FIXME: maybe below logic could be written better, as of now it is kind of hard to debug and maintain
    private void updateMapOfString(JsonObject source, String fieldNamesStr, String fieldModesStr, JsonArray jArr) {
        List<String> fNames = Arrays.asList(fieldNamesStr.split("\\.", 0));
        List<String> fModes = Arrays.asList(fieldModesStr.split("\\.", 0));
        List<JsonObject> sources = new ArrayList<>();

        // decides whether it is a normal flow or a jsonArray flow
        if (!jArr.isEmpty()) {
            for (JsonElement je : jArr) {
                sources.add(je.getAsJsonObject());
            }
        } else {
            sources = Arrays.asList(source);
        }


        for (JsonObject sourceObj : sources) {
            JsonObject latestObj = new JsonObject();
            JsonObject parentObj = sourceObj;
            boolean isJsonArrayFlow = false;

            for (int i = 0; i < fNames.size(); i++) {
                String field = fNames.get(i);
                String mode = fModes.get(i);

                if (i > 0) {
                    parentObj = latestObj;
                }

                // field mode is NULLABLE, or it is the last field in iteration.
                // field_1.field_2.field_3.field_4
                // NULLABLE.REPEATED.NULLABLE.REPEATED
                // why treat last field as NULLABLE even though in the schema it is REPEATED?
                // because it will become REPEATED after we run updateMapOfStringObj(), but not now
                // example value: {"key":"value}, later will be updated as [{"key":"key","value":"value"}] in updateMapOfStringObj
                if (Objects.equals(mode, Constant.NULLABLE) || i == fNames.size() - 1) {
                    latestObj = (latestObj.size() == 0) ? sourceObj.getAsJsonObject(field) : latestObj.getAsJsonObject(field);
                } else { // mode is REPEATED
                    JsonArray latestJArr = (latestObj.size() == 0) ? sourceObj.getAsJsonArray(field) : latestObj.getAsJsonArray(field);

                    // recursively calls updateMapOfString with fields that haven't been looped yet
                    // for example: field1.field2.field3.field4.field5
                    // if current field is field3, subList result will be field4.field5
                    updateMapOfString(new JsonObject(),
                            String.join(".", fNames.subList(i + 1, fNames.size())),
                            String.join(".", fModes.subList(i + 1, fModes.size())),
                            latestJArr);

                    isJsonArrayFlow = true;
                    break;
                }
            }

            // if current iteration is a JsonArray, then there is no object to be updated yet, hence we skip updating the object
            if (isJsonArrayFlow) {
                continue;
            }

            // every map of string field is a nested/record type,
            // so there should be no possibility of lastIndexOf returning -1
            String lastField = fieldNamesStr.substring(fieldNamesStr.lastIndexOf(".") + 1);
            parentObj.remove(lastField);
            parentObj.add(lastField, updateMapOfStringObj(latestObj));
        }
    }

    private void handleMapOfString(JsonObject fullEvent, List<TableFieldSchema> fields) {
        List<List<String>> mapOfStringFields = new ArrayList<>();
        // value example:
        // [
        //  [parent_a.child_a.child_child_a, NULLABLE.REPEATED.REPEATED],
        //  [parent_a.child_b,               NULLABLE.REPEATED]
        // ]

        // jsonObjects are updated by reference
        findMapOfStringField(mapOfStringFields, new ArrayList<>(), fields);

        for (List<String> field : mapOfStringFields) {
            String fieldNames = field.get(0); // parent_a.child_a.child_child_a
            String fieldModes = field.get(1); // NULLABLE.REPEATED.NULLABLE

            try {
                updateMapOfString(fullEvent, fieldNames, fieldModes, new JsonArray());
            } catch (NullPointerException npe) {
                // expected
            } catch (Exception e) {
                LOG.error(LogUtils.stackTraceToString(e));
            }
        }
    }

    private String TMRecordValueParser(String valueStr, String schemaStr) {
        Gson gson = new Gson();

        JsonObject fullEvent = gson.fromJson(valueStr, JsonObject.class);

        TableFieldSchema[] tfs = gson.fromJson(schemaStr, TableFieldSchema[].class);
        List<TableFieldSchema> fields = new ArrayList<>(Arrays.asList(tfs));

        handleMapOfString(fullEvent, fields);
//        LOG.info(fullEvent.toString());
        return fullEvent.toString();
    }


    @ProcessElement
    public void processElement(ProcessContext ctx) {
        KafkaRecord<String, String> record = ctx.element();
        String bqSchemaStr;

        String topic = record.getTopic();
        if (topic == null || topic.isEmpty()) {
            LOG.warn(String.format("%s: null topic found for kafka record", className));
            return;
        }

        KV<String, String> recordKV = record.getKV();
        // TODO: make sure if we really need key from kafka record
        if (recordKV.getKey() == null || recordKV.getKey().isEmpty()) {
            LOG.warn(String.format("%s: empty key for kafka record in topic %s", className, topic));
            return;
        }
        if (recordKV.getValue() == null || recordKV.getValue().isEmpty()) {
            LOG.warn(String.format("%s: empty value for kafka record in topic %s [key:%s]", className, topic, recordKV.getKey()));
            return;
        }

        try {
            JsonParser.parseString(recordKV.getValue());
        } catch (JsonSyntaxException e) {
            LOG.warn(String.format("%s: invalid json syntax in topic %s [key:%s,value:%s]", className, topic, recordKV.getKey(), recordKV.getValue()));
            return;
        }

        try {
            bqSchemaStr = SchemaUtils.getSchema(record.getTopic(), schemaDir, this.redis);
            if (bqSchemaStr.isEmpty()) {
                LOG.warn(String.format("%s: schema not found for kafka record in topic %s. [key:%s,value:%s]", className, topic, recordKV.getKey(), recordKV.getValue()));
                return;
            }
        } catch (Exception e) {
            LOG.error(String.format("%s: found exception when getting schema for kafka record in topic %s. [key:%s,value:%s]", className, topic, recordKV.getKey(), recordKV.getValue()));
            return;
        }

        TMRecord tm = new TMRecord(
                topic,
                recordKV.getKey(),
                recordKV.getValue(),
                record.getTimestamp())
                .setProcessedTimestamp(Instant.now().toEpochMilli())
                .setSchema(bqSchemaStr);

        try {
            String parsedValue = TMRecordValueParser(recordKV.getValue(), bqSchemaStr);
            tm.setValue(parsedValue);
            ctx.output(validTag, tm);

        } catch (Exception e) {
            LOG.error(String.format("%s: exception %s in topic %s [key:%s,value:%s]", className, e.toString(), topic, recordKV.getKey(), recordKV.getValue()));
            LOG.error(e.getLocalizedMessage());
            LOG.error(LogUtils.stackTraceToString(e));
            ctx.output(invalidTag, tm);
        }
    }
}

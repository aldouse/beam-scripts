package com.aldouse.transformation;

import com.aldouse.tm.TMRecord;
import com.google.api.services.bigquery.model.TableRow;
import org.apache.beam.sdk.coders.Coder;
import org.apache.beam.sdk.io.gcp.bigquery.TableRowJsonCoder;
import org.apache.beam.sdk.transforms.SerializableFunction;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;

public class TMRecordToTableRowFormatter implements SerializableFunction<TMRecord, TableRow> {

    private String partitionField;

    public static TMRecordToTableRowFormatter create() {
        return new TMRecordToTableRowFormatter();
    }

    public String getPartitionField() {
        return partitionField;
    }

    public TMRecordToTableRowFormatter setPartitionField(String partitionField) {
        this.partitionField = partitionField;
        return this;
    }

    @Override
    public TableRow apply(TMRecord input) {
        TableRow row = convertJsonToTableRow(input.getValue());
        row.set(this.partitionField, Instant.ofEpochMilli(input.getProcessedTimestamp()).toString());
        return row;
    }

    /**
     * Converts a JSON string to a {@link TableRow} object. If the data fails to convert, a {@link
     * RuntimeException} will be thrown.
     *
     * @param json The JSON string to parse.
     * @return The parsed {@link TableRow} object.
     */
    private static TableRow convertJsonToTableRow(String json) {
        TableRow row;
        // Parse the JSON into a {@link TableRow} object.
        try (InputStream inputStream = new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8))) {
            row = TableRowJsonCoder.of().decode(inputStream, Coder.Context.OUTER);
        } catch (IOException e) {
            throw new RuntimeException("Failed to serialize json to table row: " + json, e);
        }
        return row;
    }
}

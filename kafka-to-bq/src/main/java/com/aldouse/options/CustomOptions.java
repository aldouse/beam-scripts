package com.aldouse.options;

import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.options.*;

public interface CustomOptions extends PipelineOptions, StreamingOptions, SdkHarnessOptions, DataflowPipelineOptions {

    @Description("redis host")
    @Default.String("localhost")
    @Validation.Required
    String getRedisHost();

    void setRedisHost(String value);

    @Description("list of kafka brokers")
    @Default.String("localhost:9092") // broker1:9092,broker2:9092
    @Validation.Required
    String getKafkaBrokers();

    void setKafkaBrokers(String value);

    @Description("list of kafka topics to be consumed")
    @Default.String("topic1,topic2,topic3")
    @Validation.Required
    String getKafkaTopics();

    void setKafkaTopics(String value);

    @Description("kafka ConsumerConfig.GROUP_ID_CONFIG")
    @Default.String("beam_local")
    @Validation.Required
    String getKafkaConsumerConfigGroupID();

    void setKafkaConsumerConfigGroupID(String value);

    @Description("kafka ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG")
    @Default.Boolean(true)
    @Validation.Required
    Boolean getKafkaConsumerConfigEnableAutoCommitConfig();

    void setKafkaConsumerConfigEnableAutoCommitConfig(Boolean value);

    @Description("kafka ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG")
    @Default.Integer(5)
    @Validation.Required
    Integer getKafkaConsumerConfigAutoCommitIntervalMs();

    void setKafkaConsumerConfigAutoCommitIntervalMs(Integer value);

    @Description("kafka ConsumerConfig.AUTO_OFFSET_RESET_CONFIG")
    @Default.String("latest")
    @Validation.Required
    String getKafkaConsumerConfigAutoOffsetReset();

    void setKafkaConsumerConfigAutoOffsetReset(String value);

    @Description("topic bq schemas location")
    @Default.String("schemas")
    @Validation.Required
    String getTopicBQSchemasLocation();

    void setTopicBQSchemasLocation(String value);


    @Description("default gcs bucket")
    @Default.String("bucket-name")
    @Validation.Required
    String getDefaultGcsBucket();

    void setDefaultGcsBucket(String value);

    @Description("gcs datalake location")
    @Default.String("datalake")
    @Validation.Required
    String getGcsDatalakeLocation();

    void setGcsDatalakeLocation(String value);

    @Description("gcs invalid location")
    @Default.String("invalid/output/gcs")
    @Validation.Required
    String getGcsInvalidLocation();

    void setGcsInvalidLocation(String value);

    @Description("gcs bq failed inserts location")
    @Default.String("failed")
    @Validation.Required
    String getGcsBQFailedLocation();

    void setGcsBQFailedLocation(String value);

    @Description("bigquery project id")
    @Default.String("project-id")
    @Validation.Required
    String getBqProjectID();

    void setBqProjectID(String value);

    @Description("bigquery dataset id")
    @Default.String("dataset-id")
    @Validation.Required
    String getBqDatasetID();

    void setBqDatasetID(String value);

    @Description("bigquery default table partition type")
    @Default.String("DAY")
    @Validation.Required
    String getBqPartitionType();

    void setBqPartitionType(String value);

    @Description("bigquery default table partition field")
    @Default.String("processed_timestamp")
    @Validation.Required
    String getBqPartitionField();

    void setBqPartitionField(String value);

    @Description("bq load job interval in seconds")
    @Default.Integer(60)
    @Validation.Required
    Integer getBqLoadJobIntervalSeconds();

    void setBqLoadJobIntervalSeconds(Integer value);


}

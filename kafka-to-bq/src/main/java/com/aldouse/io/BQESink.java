package com.aldouse.io;

import com.aldouse.wrapper.BigQueryInsertErrorWrapper;
import com.google.gson.Gson;
import org.apache.beam.sdk.io.FileIO;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;

public class BQESink implements FileIO.Sink<BigQueryInsertErrorWrapper>{

    private PrintWriter writer;

    public BQESink() {}

    public void open(@NonNull WritableByteChannel channel) throws IOException {
        writer = new PrintWriter(Channels.newOutputStream(channel));
    }

    public void write(BigQueryInsertErrorWrapper element) throws IOException {
        assert element != null;
        writer.println(element.getRowJson());
    }

    public void flush() throws IOException {
        writer.flush();
    }
}

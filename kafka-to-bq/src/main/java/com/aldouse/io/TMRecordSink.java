package com.aldouse.io;

import com.aldouse.tm.TMRecord;
import org.apache.beam.sdk.io.FileIO;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;

public class TMRecordSink implements FileIO.Sink<TMRecord>{

    private PrintWriter writer;

    public TMRecordSink() {}

    public void open(@NonNull WritableByteChannel channel) throws IOException {
        writer = new PrintWriter(Channels.newOutputStream(channel));
    }

    public void write(TMRecord element) throws IOException {
        assert element != null;
        writer.println(element.getValue());
    }

    public void flush() throws IOException {
        writer.flush();
    }
}

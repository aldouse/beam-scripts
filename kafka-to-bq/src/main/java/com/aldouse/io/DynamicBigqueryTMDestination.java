package com.aldouse.io;

import com.aldouse.tm.TMRecord;
import com.aldouse.wrapper.BigQueryDestination;
import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.api.services.bigquery.model.TimePartitioning;
import com.google.cloud.bigquery.storage.v1.TableFieldSchemaOrBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.beam.sdk.io.gcp.bigquery.DynamicDestinations;
import org.apache.beam.sdk.io.gcp.bigquery.TableDestination;
import org.apache.beam.sdk.values.ValueInSingleWindow;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DynamicBigqueryTMDestination extends DynamicDestinations<TMRecord, BigQueryDestination> {
    private String projectID;
    private String datasetID;
    private final String partitionType;
    private final String partitionField;
    private Map<String, String> tableSchemas; // <topic name, bq schema>

    public DynamicBigqueryTMDestination(String projectID, String datasetID, String partitionType, String partitionField) {
        this.projectID = projectID;
        this.datasetID = datasetID;
        this.partitionType = partitionType;
        this.partitionField = partitionType;
    }

    private String convertTopicToBQTableID(String topic) {
        String tableName = topic.replace(".", "__");
        return this.projectID + "." + this.datasetID + "." + tableName;
    }

    @Override
    public BigQueryDestination getDestination(ValueInSingleWindow<TMRecord> element) {
        TMRecord e = element.getValue();
        return new BigQueryDestination(e.getSchema(), e.getTopic() );
    }

    @Override
    public TableDestination getTable(BigQueryDestination bd) {
        TimePartitioning timePartitioning = new TimePartitioning()
                .setType(this.partitionType)
                .setField(this.partitionField);

        return new TableDestination(
                convertTopicToBQTableID(bd.getKey()),
                "Table for topic " + bd.getKey()
//                TODO: cannot create parititoned table, fix this
//               , timePartitioning
        );
    }

    @Override
    public TableSchema getSchema(BigQueryDestination bd) {
        Gson gson = new Gson();

        TableFieldSchema[] tfs = gson.fromJson(bd.getSchema(), TableFieldSchema[].class);
        List<TableFieldSchema> listTfs = new ArrayList<>(Arrays.asList(tfs));

        return new TableSchema().setFields(listTfs);
    }
}

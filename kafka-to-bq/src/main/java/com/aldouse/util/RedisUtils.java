package com.aldouse.util;

import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Collections;
import java.util.Map;

public class RedisUtils {
    static Logger LOG = LoggerFactory.getLogger(RedisUtils.class);
    public JedisPool pool = null;
    Map<String, String> localCache;
    Map<String, Map<String, String>> localCacheMap;
    private String host;
    private String prefix;

    public RedisUtils() {}

    public RedisUtils(String host) {
        this.host = host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setLocalCache(String prefix, long ttlMillis) {
        this.prefix = prefix;
        localCache = Collections.synchronizedMap(new PassiveExpiringMap<>(ttlMillis));
        localCacheMap = Collections.synchronizedMap(new PassiveExpiringMap<>(ttlMillis));
    }

    public void initRedis() {
        try {
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setMaxWaitMillis(180000);
            this.pool = new JedisPool(jedisPoolConfig, this.host, 6379, 180000);
        } catch (Exception e) {
            LOG.error(String.format("[Redis] Failed to make connection: %s", LogUtils.stackTraceToString(e)));
        }
    }

    public void setToRedis(String key, String value) {
        try (Jedis jedis = pool.getResource()) {
            jedis.set(key, value);
            jedis.expire(key, 3600);
        } catch (Exception e) {
            LOG.error(String.format("[Redis] Failed to set key %s", LogUtils.stackTraceToString(e)));
        }
    }

    public String getFromRedis(String key) {
        // beware of duplicate key
        String cachedKey = String.format("%s:%s", prefix, key);
        String output = localCache.get(cachedKey);
        if (output != null && !output.isEmpty()) {
            return output;
        }

        try (Jedis jedis = pool.getResource()) {
            output = jedis.get(key);
        } catch (Exception e) {
            LOG.error(String.format("[Redis] Failed to retrieve %s", LogUtils.stackTraceToString(e)));
        }
        localCache.put(cachedKey, output);
        return output;
    }

    public void deleteRedis(String key) {
        try (Jedis jedis = pool.getResource()) {
            jedis.del(key);
        } catch (Exception e) {
            LOG.error(String.format("[Redis] Failed to delete key %s: %s", key, LogUtils.stackTraceToString(e)));
        }

    }



}

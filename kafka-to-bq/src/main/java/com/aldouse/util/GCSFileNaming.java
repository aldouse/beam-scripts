package com.aldouse.util;

import com.aldouse.transformation.ConvertKafkaRecordToTMRecord;
import org.apache.beam.sdk.io.Compression;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.transforms.windowing.BoundedWindow;
import org.apache.beam.sdk.transforms.windowing.PaneInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class GCSFileNaming implements FileIO.Write.FileNaming {
    static Logger LOG = LoggerFactory.getLogger(GCSFileNaming.class);
    private final String prefix;
    private final String suffix;

    public GCSFileNaming(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public static GCSFileNaming getNaming(String prefix, String suffix) {
        return new GCSFileNaming(prefix, suffix);
    }

    @Override
    public String getFilename(BoundedWindow window,
                              PaneInfo pane,
                              int numShards,
                              int shardIndex,
                              Compression compression) {
        return String.format(
                "%s/file-%s-%d-%d.%s",
                prefix,
                UUID.randomUUID().toString(),
                shardIndex,
                numShards,
                suffix
        );
    }
}

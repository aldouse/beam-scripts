package com.aldouse.util;

import org.apache.beam.sdk.io.FileSystems;
import org.apache.beam.sdk.io.fs.ResourceId;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;

public class SchemaUtils {

    static Logger LOG = LoggerFactory.getLogger(SchemaUtils.class);

    public static String getBQSchemaFromGCS(String topic, String schemaDir) {
        String schema = "";
        String schemaFile = String.format("%s/%s.schema", schemaDir, topic);
        ResourceId resourceId = FileSystems.matchNewResource(schemaFile, false);

        try {
            InputStream in = Channels.newInputStream(FileSystems.open(resourceId));
            byte[] b = IOUtils.toByteArray(in);
            schema = new String(b);
            in.close();
        } catch (FileNotFoundException e) {
            LOG.error(String.format("Schema file %s not found : %s", topic, LogUtils.stackTraceToString(e)));
        } catch (IOException e) {
            LOG.error(String.format("Error reading schema file %s : %s", topic, LogUtils.stackTraceToString(e)));
        }
        return schema;
    }

    public static String getSchema(String topic, String schemaDir, RedisUtils redis) {
        String schema = "";

        try {
            schema = redis.getFromRedis(topic);
        } catch (Exception e) {
            LOG.error(String.format("[Redis] Failed to get value: %s", LogUtils.stackTraceToString(e)));
        }

        if (schema == null || schema.equals("")) {
            schema = getBQSchemaFromGCS(topic, schemaDir);
            if (!schema.isEmpty()) {
                try {
                    redis.setToRedis(topic, schema);
                } catch (Exception e) {
                    LOG.error(String.format("[Redis] Failed to set value: %s", LogUtils.stackTraceToString(e)));
                }
            }
        }
        return schema;
    }

}

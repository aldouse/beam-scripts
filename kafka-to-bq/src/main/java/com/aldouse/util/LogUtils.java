package com.aldouse.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class LogUtils {

    public static String stackTraceToString(final Throwable throwable) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        return sw.getBuffer().toString();
    }
}
